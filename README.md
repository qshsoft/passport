qshsoft/passport

## 安装

~~~
composer require qshsoft/passport @dev
~~~
## 使用

~~~
 * 使用示例：
 * //初始化 Passport 类
 * $passport = new PassportV1(['Domain' => 'passport.test.pai2345.com', 'Host' => '192.168.1.7', 'AppId' => 'web', 'AppSceret' => '32e914762436769432e71ca74485ad8d']);
 * //极验验证初始化
 * $passport->captchaGeetest();
 * //登录
 * $passport->login("test1", "123456", "02899f04a0a6b3d058901e505b46aa8cl5", "14c17be31242a350712ad77171938a3f","14c17be31242a350712ad77171938a3f|jordan", "639e921fdcc0500a1da2ddab2eb58433");
 * //二次验证
 * $passport->verify("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9wYXNzcG9ydC53b3JrLnlxeHN5LmNvbVwvd2ViXC9hdXRoXC9sb2dpbiIsImlhdCI6MTU4MjEwNjY0NSwiZXhwIjoxNTgyMTEwMjQ1LCJuYmYiOjE1ODIxMDY2NDUsImp0aSI6Ikw5U1llOWRnWkdUVlNmWU4iLCJzdWIiOjY5NSwicHJ2IjoiMjNiZDVjODk0OWY2MDBhZGIzOWU3MDFjNDAwODcyZGI3YTU5NzZmNyJ9.7V5pYJMPo2s9kO4ed36Ypn0Ckr6ijvquCQNMydv8O7s");
 * //短信发送
 * $passport->captchaSms(13505911234, PassportV1::SMS_SCENARIO_REGISTER, 1);
 * //登出
 * $passport->logout();
 * //注册
 * $passport->register("test7", "123456", "13405940131", "天天天", "23fdff0c104fe26174c1a21f38fb52cf", "1234", 1);
 * //获取token
 * $passport->getCookieToken();
 * //校验token
 * $passport->verifyToken();
  
~~~


## 目录结构
```
.
├── README.md
├── composer.json
├── docs
├── src
    └── PassportV1.php
```
