<?php
/**
 * 用户中心类
 * User: kentChen
 * Date: 2020/2/3
 * Time: 11:05
 *
 * 使用示例：
 * //初始化 Passport 类
 * $passport = new PassportV1(['Domain' => 'passport.work.pai2345.com', 'Host' => '192.168.1.119', 'AppId' => 'web', 'AppSceret' => '32e914762436769432e71ca74485ad8d']);
 * //极验验证初始化
 * $passport->captchaGeetest();
 * //登录
 * $passport->login("test1", "123456", "02899f04a0a6b3d058901e505b46aa8cl5", "14c17be31242a350712ad77171938a3f","14c17be31242a350712ad77171938a3f|jordan", "639e921fdcc0500a1da2ddab2eb58433");
 * //二次验证
 * $passport->verify("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9wYXNzcG9ydC53b3JrLnlxeHN5LmNvbVwvd2ViXC9hdXRoXC9sb2dpbiIsImlhdCI6MTU4MjEwNjY0NSwiZXhwIjoxNTgyMTEwMjQ1LCJuYmYiOjE1ODIxMDY2NDUsImp0aSI6Ikw5U1llOWRnWkdUVlNmWU4iLCJzdWIiOjY5NSwicHJ2IjoiMjNiZDVjODk0OWY2MDBhZGIzOWU3MDFjNDAwODcyZGI3YTU5NzZmNyJ9.7V5pYJMPo2s9kO4ed36Ypn0Ckr6ijvquCQNMydv8O7s");
 * //短信发送
 * $passport->captchaSms(13505911234, PassportV1::SMS_SCENARIO_REGISTER, 1);
 * //登出
 * $passport->logout();
 * //注册
 * $passport->register("test7", "123456", "13405940131", "天天天", "23fdff0c104fe26174c1a21f38fb52cf", "1234", 1);
 * //获取token
 * $passport->getCookieToken();
 * //校验token
 * $passport->verifyToken();
 *
 * //内部接口 - 登录
 * $passport->internal_login("13505910001", "123456");
 * //内部接口 - 注册
 * $passport->internal_register(['username' => '15059001006', 'password' => '123456', 'mobile' => '15059001006', 'is_login' => 1]);
 * //内部接口 - 修改用户资料
 * $passport->internal_update(['user_id' => 27264, 'nickname' => "服务端昵称", 'email' => 'test@test.com']);
 * //内部接口 - 获取用户途币
 * $passport->internal_getCoin(27283);
 * //内部接口 - 途币操作接口
 * $passport->internal_coinOperation(27283, 2, 100, 2, 27264, '打赏途币');
 * //内部接口 - 途币流水列表
 * $passport->internal_coinSearch(['user_id' => 27283, 'page' => 3, 'pageSize' => 5]);
 * //短信校验
 * $passport->internal_captchaSmsVerify("fee46d12723ebb10cbd4f239ba9d528d", "154874","13405947557", 1);
 *
 * iss: jwt签发者
 * sub: jwt所面向的用户
 * aud: 接收jwt的一方
 * exp: jwt的过期时间，过期时间必须要大于签发时间
 * nbf: 定义在什么时间之前，某个时间点后才能访问
 * iat: jwt的签发时间
 * jti: jwt的唯一身份标识，主要用来作为一次性token。
 *
 */

namespace Qshsoft\passport;

use Lcobucci\JWT\ValidationData;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Signer\Hmac\Sha256;

class PassportV1
{
    const CURL_GET = "get";
    const CURL_POST = "post";

    const SMS_SCENARIO_REGISTER = "register";

    //通信协议
    protected $Scheme = '';
    //用户中心域名
    protected $Domain = 'passport.test.pai2345.com';
    //HOST 绑定 ip
    protected $Host = '';
    protected $AccessToken = '';
    protected $AppId = '';
    protected $AppSceret = '';

    public function __construct($config = null)
    {
        if (empty($config)) {
            return false;
        }
        $this->Domain = $config['Domain'];
        $this->Scheme = empty($config['Scheme']) ? 'http' : $config['Scheme'];
        $this->Host = empty($config['Host']) ? '' : $config['Host'];
        $this->AppId = $config['AppId'];
        $this->AppSceret = $config['AppSceret'];
        $this->JwtSceret = $config['AppSceret'];
    }

    /**
     * 获取社会化登录地址
     * @param string $type 目前只支持微信
     * @param string $target_url  回调地址
     * @param string $is_ajax 是否异步
     * @return string
     */
    public function social_login_url($type = 'wechat', $target_url, $is_ajax = 0)
    {
        $uri  = 'web/social/'.$type.'/login';
        $data = [
            'target_url' => $target_url,
            'app_id' => $this->AppId,
            'is_ajax' => $is_ajax
        ];
        $url = $this->Scheme.'://'. $this->Domain . '/' . $uri . '?' . http_build_query($data);
        return $url;
    }

    /**
     * 内部接口 - 登录
     * @param $username 用户名
     * @param $password 密码
     * @return array
     */
    public function internal_login($username, $password)
    {
        $uri  = 'internal/user/login';
        $data = [
            'app_id' => $this->AppId,
            'timestamp' => time(),
            'username' => $username,
            'password' => $password,
        ];
        $data['sign'] = $this->createSign($data);
        try{
            $response = $this->curl(self::CURL_POST, $uri, $data);
            $this->AccessToken = $response['data']['access_token'];
        } catch (\Exception $e) {
            return $this->responseData(false, $e->getMessage());
        }
        return $this->responseData(true, '', $response['data']);
    }
    
    /**
     * 内部接口 - 登录 by user_id
     * @param $user_id 用户id
     * @return array
     */
    public function internal_login_id($user_id)
    {
        $uri  = 'internal/user/login_id';
        $data = [
            'app_id' => $this->AppId,
            'timestamp' => time(),
            'user_id' => $user_id,
        ];
        $data['sign'] = $this->createSign($data);
        try{
            $response = $this->curl(self::CURL_POST, $uri, $data);
            $this->AccessToken = $response['data']['access_token'];
        } catch (\Exception $e) {
            return $this->responseData(false, $e->getMessage());
        }
        return $this->responseData(true, '', $response['data']);
    }

    /**
     * 内部接口 - 登录 by mobile
     * @param $mobile 手机号
     * @return array
     */
    public function internal_login_mobile($mobile)
    {
        $uri  = 'internal/user/login_mobile';
        $data = [
            'app_id' => $this->AppId,
            'timestamp' => time(),
            'mobile' => $mobile,
        ];
        $data['sign'] = $this->createSign($data);
        try{
            $response = $this->curl(self::CURL_POST, $uri, $data);
            $this->AccessToken = $response['data']['access_token'];
        } catch (\Exception $e) {
            return $this->responseData(false, $e->getMessage());
        }
        return $this->responseData(true, '', $response['data']);
    }

    /**
     * 内部接口 - 获取用户信息
     * @param $field 字段名  支持 id,username,mobile
     * @param $value 值
     * @return array
     */
    public function internal_user_info($field, $value)
    {
        $uri  = 'internal/user/info';
        $data = [
            'app_id' => $this->AppId,
            'timestamp' => time(),
            'field' => $field,
            'value' => $value,
        ];
        $data['sign'] = $this->createSign($data);
        try{
            $response = $this->curl(self::CURL_POST, $uri, $data);
        } catch (\Exception $e) {
            return $this->responseData(false, $e->getMessage());
        }
        return $this->responseData(true, '', empty($response['data']) ? '' : $response['data']);
    }

    /**
     * 内部接口 - 微信登录
     * @param $wx_unionid 微信唯一id
     * @return array
     */
    public function internal_login_wechat($wx_unionid)
    {
        $uri  = 'internal/user/login_wechat';
        $data = [
            'app_id' => $this->AppId,
            'timestamp' => time(),
            'wx_unionid' => $wx_unionid,
        ];
        $data['sign'] = $this->createSign($data);
        try{
            $response = $this->curl(self::CURL_POST, $uri, $data);
            $this->AccessToken = $response['data']['access_token'];
        } catch (\Exception $e) {
            return $this->responseData(false, $e->getMessage());
        }
        return $this->responseData(true, '', $response['data']);
    }

    /**
     * 内部接口 - 注册
     * @param $params
     * @return array
     */
    public function internal_register($params)
    {
        $uri  = 'internal/user/register';
        $fields = [
            'username',
            'password',
            'salt',
            'mobile',
            'nickname',
            'realname',
            'wx_unionid',
            'email',
            'avatar',
            'country',
            'province',
            'city',
            'county',
            'address',
            'postcode',
            'register_platform',
            'is_login',
        ];
        $data = [
            'app_id' => $this->AppId,
            'timestamp' => time(),
        ];
        foreach ($fields as $field) {
            if (isset($params[$field])) {
                $data[$field] = $params[$field];
            }
        }
        unset($params);
        $data['sign'] = $this->createSign($data);
        try{
            $response = $this->curl(self::CURL_POST, $uri, $data);
        } catch (\Exception $e) {
            return $this->responseData(false, $e->getMessage());
        }
        return $this->responseData(true, $response['message'], empty($response['data']) ? '' : $response['data']);
    }

    /**
     * 内部接口 - 修改用户资料
     * @param $params
     * @return array
     */
    public function internal_update($params)
    {
        if (!is_array($params)) {
            return $this->responseData(false, 'params参数必须是数组');
        }
        $uri  = 'internal/user/update';
        $fields = [
            'user_id',
            'username',
            'mobile',
            'password',
            'salt',
            'realname',
            'nickname',
            'wx_unionid',
            'email',
            'avatar',
            'country',
            'province',
            'city',
            'county',
            'address',
            'postcode',
            'register_platform',
        ];
        $data = [
            'app_id' => $this->AppId,
            'timestamp' => time(),
        ];
        foreach ($fields as $field) {
            if (isset($params[$field])) {
                $data[$field] = $params[$field];
            }
        }
        unset($params);
        $data['sign'] = $this->createSign($data);
        try{
            $response = $this->curl(self::CURL_POST, $uri, $data);
        } catch (\Exception $e) {
            return $this->responseData(false, $e->getMessage());
        }
        return $this->responseData(true, $response['message'], empty($response['data']) ? '' : $response['data']);
    }

    /**
     * 内部接口 - 获取用户途币
     * @param $user_id
     * @return array
     */
    public function internal_getCoin($user_id)
    {
        $uri  = 'internal/coin/getCoin';
        $data = [
            'user_id' => $user_id,
            'app_id' => $this->AppId,
            'timestamp' => time(),
        ];
        $data['sign'] = $this->createSign($data);
        try{
            $response = $this->curl(self::CURL_POST, $uri, $data);
        } catch (\Exception $e) {
            return $this->responseData(false, $e->getMessage());
        }
        return $this->responseData(true, $response['message'], empty($response['data']) ? '' : $response['data']);
    }

    /**
     * 内部接口 - 途币流水列表
     * @param $params
     * @return array
     */
    public function internal_coinSearch($params = [])
    {
        if (!is_array($params)) {
            return $this->responseData(false, 'params参数必须是数组');
        }
        $uri  = 'internal/coin/search';
        $fields = [
            'user_id',
            'type',
            'source_id',
            'orderby_field',
            'orderby_sort',
            'page',
            'pageSize',
        ];
        $data = [
            'app_id' => $this->AppId,
            'timestamp' => time(),
        ];
        foreach ($fields as $field) {
            if (isset($params[$field])) {
                $data[$field] = $params[$field];
            }
        }
        unset($params);
        $data['sign'] = $this->createSign($data);
        try{
            $response = $this->curl(self::CURL_POST, $uri, $data);
        } catch (\Exception $e) {
            return $this->responseData(false, $e->getMessage());
        }
        return $this->responseData(true, $response['message'], empty($response['data']) ? '' : $response['data']);
    }

    /**
     * 内部接口 - 途币操作接口
     * @param $user_id
     * @param $type
     * @param $coin
     * @param $source_id
     * @param $source_no
     * @param string $remark
     * @return array
     */
    public function internal_coinOperation($user_id, $type, $coin, $source_id, $source_no, $remark = '')
    {
        $uri  = 'internal/coin/operation';
        $data = [
            'user_id' => $user_id,
            'type' => $type,
            'coin' => $coin,
            'source_id' => $source_id,
            'source_no' => $source_no,
            'remark' => $remark,
            'app_id' => $this->AppId,
            'timestamp' => time(),
        ];
        $data['sign'] = $this->createSign($data);
        try{
            $response = $this->curl(self::CURL_POST, $uri, $data);
        } catch (\Exception $e) {
            return $this->responseData(false, $e->getMessage());
        }
        return $this->responseData(true, $response['message'], empty($response['data']) ? '' : $response['data']);
    }

    /**
     * 短信验证
     * @param $sms_key   验证key
     * @param $sms_code  验证码
     * @param $mobile    手机号
     * @param $scenario  场景 参考APP短信验证配置
     * @return array
     */
    public function internal_captchaSmsVerify($sms_key, $sms_code, $mobile, $scenario)
    {
        $uri  = 'internal/captcha/smsVerify';
        try{
            $data = [
                'sms_key' => $sms_key,
                'sms_code' => $sms_code,
                'mobile' => $mobile,
                'scenario' => $scenario,
                'app_id' => $this->AppId,
                'timestamp' => time(),
            ];
            $data['sign'] = $this->createSign($data);
            $response = $this->curl(self::CURL_POST, $uri, $data);
        } catch (\Exception $e) {
            return $this->responseData(false, $e->getMessage());
        }
        return $this->responseData(true, $response['message'], empty($response['data']) ? '' : $response['data']);
    }

    /**
     * 二次校验
     * @param $accessToken JWT生成的token
     * @param $timestamp 当前时间戳
     * @return array
     */
    public function verify($accessToken = null, $timestamp = null)
    {
        if (empty($accessToken)) {
            $accessToken = $this->getCookieToken();
        }
        $uri = 'web/auth/verify';
        $data = [
            'app_id' => $this->AppId,
            'timestamp' => $timestamp ?? time()
        ];
        $data['sign'] = $this->createSign($data);
        $this->AccessToken = $accessToken;
        try{
            $response = $this->curl(self::CURL_POST, $uri, $data);
        } catch (\Exception $e) {
            return $this->responseData(false, $e->getMessage());
        }
        return $this->responseData(true, '', $response['data']);
    }

    /**
     * 注册
     * @param $username 用户名
     * @param $password 密码
     * @param $mobile 手机号
     * @param $nickname 昵称
     * @param $sms_key 短信验证key
     * @param $sms_code 短信验证码
     * @param $is_login 注册并登录 0 否 1 是
     * @return array
     */
    public function register($username, $password, $mobile, $nickname, $sms_key, $sms_code, $is_login = 0)
    {
        $uri  = 'web/auth/register';
        $data = [
            'username' => $username,
            'password' => $password,
            'mobile' => $mobile,
            'nickname' => $nickname,
            'sms_key' => $sms_key,
            'sms_code' => $sms_code,
            'is_login' => $is_login,
        ];
        try{
            $response = $this->curl(self::CURL_POST, $uri, $data);
        } catch (\Exception $e) {
            return $this->responseData(false, $e->getMessage());
        }
        return $this->responseData(true, $response['message'], empty($response['data']) ? '' : $response['data']);
    }

    /**
     * 忘记密码
     * @param $mobile 手机号
     * @param $password 密码
     * @param $sms_key 短信验证key
     * @param $sms_code 短信验证码
     * @return array
     */
    public function forgetpwd($mobile, $password,  $sms_key, $sms_code)
    {
        $uri  = 'web/auth/forgetpwd';
        $data = [
            'username' => $mobile, //这里username 和 mobile 是一样的
            'password' => $password,
            'sms_key' => $sms_key,
            'sms_code' => $sms_code
        ];
        try{
            $response = $this->curl(self::CURL_POST, $uri, $data);
        } catch (\Exception $e) {
            return $this->responseData(false, $e->getMessage());
        }
        return $this->responseData(true, $response['message'], empty($response['data']) ? '' : $response['data']);
    }

    /**
     * 登录
     * @param $mobile 手机号
     * @param $sms_key 短信验证key
     * @param $sms_code 短信验证码
     * @return array
     */
    public function login_mobile($mobile, $sms_key, $sms_code)
    {
        $uri  = 'web/auth/login_mobile';
        $data = [
            'mobile' => $mobile,
            'sms_key' => $sms_key,
            'sms_code' => $sms_code
        ];
        try{
            $response = $this->curl(self::CURL_POST, $uri, $data);
            $this->AccessToken = $response['data']['access_token'];
        } catch (\Exception $e) {
            return $this->responseData(false, $e->getMessage());
        }
        return $this->responseData(true, '', $response['data']);
    }

    /**
     * 登录
     * @param $username 用户名
     * @param $password 密码
     * @param $geetest_challenge 极验前端生成
     * @param $geetest_validate 极验前端生成
     * @param $geetest_seccode 极验前端生成
     * @param $geetest_key 服务端极验初始化接口返回
     * @return array
     */
    public function login($username, $password, $geetest_challenge, $geetest_validate, $geetest_seccode, $geetest_key)
    {
        $uri  = 'web/auth/login';
        $data = [
            'username' => $username,
            'password' => $password,
            'geetest_challenge' => $geetest_challenge,
            'geetest_validate' => $geetest_validate,
            'geetest_seccode' => $geetest_seccode,
            'geetest_key' => $geetest_key
        ];
        try{
            $response = $this->curl(self::CURL_POST, $uri, $data);
            $this->AccessToken = $response['data']['access_token'];
        } catch (\Exception $e) {
            return $this->responseData(false, $e->getMessage());
        }
        return $this->responseData(true, '', $response['data']);
    }

    /**
     * 退出
     * @param $accessToken JWT生成的token
     * @return array
     */
    public function logout($accessToken = null)
    {
        $uri  = 'web/auth/logout';
        try{
            if (empty($accessToken)) {
                $accessToken = $this->getCookieToken();
            }
            $response = $this->curl(self::CURL_POST, $uri, ['access_token' => $accessToken]);
            $this->AccessToken = '';
        } catch (\Exception $e) {
            return $this->responseData(false, $e->getMessage());
        }
        return $this->responseData(true, $response['message']);
    }

    /**
     * 短信发送
     * @param $mobile  手机号
     * @param $scenario 场景 参考APP短信验证配置
     * @param int $debug 0关闭 1开启时 不发送短信，验证码固定为 1234
     * @return array
     */
    public function captchaSms($mobile, $scenario, $debug = 0)
    {
        $uri = 'web/captcha/sms';
        try{
            $data = [
                'mobile' => $mobile,
                'scenario' => $scenario,
                'debug' => $debug,
            ];
            $response = $this->curl(self::CURL_POST, $uri, $data);
        } catch (\Exception $e) {
            return $this->responseData(false, $e->getMessage());
        }
        return $this->responseData(true, '', $response['data']);
    }

    /**
     * 极验验证初始化
     * @return array
     */
    public function captchaGeetest()
    {
        $uri = 'web/captcha/geetest';
        try{
            $response = $this->curl(self::CURL_GET, $uri);
        } catch (\Exception $e) {
            return $this->responseData(false, $e->getMessage());
        }
        return $this->responseData(true, '', $response['data']);
    }

    /**
     * 获取cookie token
     * @return string
     */
    public function getCookieToken()
    {
        return empty($_COOKIE['access_token']) ? '' : $_COOKIE['access_token'];
    }

    /**
     * 校验token是否有效
     * @return string
     */
    public function verifyToken($accessToken = null)
    {
        if (empty($accessToken)) {
            $accessToken = $this->getCookieToken();
        }
        if (empty($accessToken)) {
            return $this->responseData(false, "token为空");
        }
        $token = (new Parser())->parse($accessToken);
        $signer = new Sha256();
        $iss = $token->getClaim('iss');
        $exp = $token->getClaim('exp');
        $nbf = $token->getClaim('nbf');
        $currTime = time();

        if (!$token->verify($signer, $this->JwtSceret)) {
            return $this->responseData(false, "token校验失败");
        }
        //签发时间大于当前服务器时间验证失败
        if (intval($iss) > $currTime) {
            return $this->responseData(false, "签发时间大于当前服务器时间");
        }
        //过期时间小于当前服务器时间验证失败
        if (intval($exp) < $currTime) {
            return $this->responseData(false, "过期时间小于当前服务器时间");
        }
        //该nbf时间之前不接收处理该Token
        if (intval($nbf) > $currTime) {
            return $this->responseData(false, "该nbf时间之前不接收处理该Token");
        }
        return $this->responseData(true, "验证成功");
    }

    /**
     * 返回数据
     * @param $success
     * @param string $message
     * @param string $data
     * @return array
     */
    protected function responseData($success, $message = '', $data = '')
    {
        return [
            'success' => $success,
            'message' => $message,
            'data' => $data,
        ];
    }

    /**
     * 生成签名
     * @param $data
     * @return string
     */
    protected function createSign($data)
    {
        if (isset($data['sign'])) {
            unset($data['sign']);
        }
        ksort($data);
        $str = '';
        foreach ($data as $k => $v) {
            $str .= $k . $v;
        }
        return md5(md5($str) . $this->AppSceret);
    }

    protected function curl($method, $uri, $data = null)
    {
        $handle = curl_init();
        $headers = [];
        if (empty($this->Host)) {
            $url = $this->Scheme.'://'. $this->Domain . '/' . $uri;
            curl_setopt($handle,CURLOPT_URL, $url);
        } else {
            $url = $this->Scheme.'://'.$this->Host.'/' . $uri;
            $headers[] = "Host: ". $this->Domain;
        }

        if (empty($this->AccessToken)) {
            $this->AccessToken = $this->getCookieToken();
        }
        if ($this->AccessToken) {
            $headers[] = "Authorization: Bearer " . $this->AccessToken;
        }
        curl_setopt($handle,CURLOPT_URL, $url);
        curl_setopt($handle,CURLOPT_HTTPHEADER, $headers);
        curl_setopt($handle,CURLOPT_RETURNTRANSFER, true);
        curl_setopt($handle,CURLOPT_HEADER, 0);
        curl_setopt($handle, CURLOPT_TIMEOUT, 10);
        if ($method == self::CURL_POST) {
            $headers[] = "Content-Type: application/x-www-form-urlencoded";
            curl_setopt($handle,CURLOPT_POST, true);
            curl_setopt($handle,CURLOPT_POSTFIELDS, http_build_query($data));
        }
        curl_setopt($handle,CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($handle,CURLOPT_SSL_VERIFYPEER, 0);
        $response_json = curl_exec($handle);
        $response = json_decode($response_json, true);
        curl_close($handle);

        if (empty($response)) {
            throw new \Exception("请求异常");
        }
        if ($response['code'] != 200 || $response['status'] != 'success') {
//            throw new \Exception("code:". $response['code'] . ", message: ". $response['message']);
            throw new \Exception($response['message']);
        }
        return $response;
    }

}